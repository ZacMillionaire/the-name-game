
#-----Statement of Authorship----------------------------------------#
#
#  By submitting this task the signatories below agree that it
#  represents our own work and that we both contributed to it.  We
#  are aware of the University rule that a student must not
#  act in a manner which constitutes academic dishonesty as stated
#  and explained in QUT's Manual of Policies and Procedures,
#  Section C/5.3 "Academic Integrity" and Section E/2.1 "Student
#  Code of Conduct".
#
#  First student's no: n8824088
#  First student's name: Scott Schultz
#  Portfolio contribution: 100%
#
#  Second student's no: Solo Submission
#
#  Contribution percentages refer to the whole portfolio, not just this
#  task.  Percentage contributions should sum to 100%.  A 50/50 split is
#  NOT necessarily expected.  The percentages will not affect your marks
#  except in EXTREME cases.
#
#--------------------------------------------------------------------#



#-----Task Description-----------------------------------------------#
#
#  THE NAME GAME
#
#  In this task you and your partner will develop a function that
#  involves manipulating text strings.  This function will produce
#  verses for the song "The Name Game" which was a hit for Shirley
#  Ellis in 1965.  To complete the task your function must pass
#  several unit tests which appear below.  See the instructions
#  accompanying this file for further details and the following
#  video for the song itself:
#
#      http://www.youtube.com/watch?v=5MJLi5_dyn0
#
#--------------------------------------------------------------------#



#-----Acceptance Tests-----------------------------------------------#
#
#  This section contains the unit tests that your program must
#  pass.  You may not change anything in this section.  NB: When
#  your program is marked the following tests will all be used as
#  well as some additional tests (not provided) to ensure your
#  solution works for other cases.
#
"""

>>> print name_game('Katie', 'K') # Test 1: Single letter as first consonant
Katie!
Katie, Katie, bo-batie,
Banana-fana fo-fatie,
Fee-Fi-mo-matie,
Katie!

>>> print name_game('Shirley', 'Sh') # Test 2: First consonant sound is two letters
Shirley!
Shirley, Shirley, bo-birley,
Banana-fana fo-firley,
Fee-Fi-mo-mirley,
Shirley!

>>> print name_game('Arnold', '') # Test 3: No first consonant
Arnold!
Arnold, Arnold, bo-barnold,
Banana-fana fo-farnold,
Fee-Fi-mo-marnold,
Arnold!

>>> print name_game('Billy', 'B') # Test 4: Initial B
Billy!
Billy, Billy, bo-illy,
Banana-fana fo-filly,
Fee-Fi-mo-milly,
Billy!

>>> print name_game('Fred', 'Fr') # Test 5: Initial F in consonant sound 'Fr'
Fred!
Fred, Fred, bo-bed,
Banana-fana fo-red,
Fee-Fi-mo-med,
Fred!

>>> print name_game('Brian', 'Br') # Test 6: Initial B in consonant sound 'Br'
Brian!
Brian, Brian, bo-rian,
Banana-fana fo-fian,
Fee-Fi-mo-mian,
Brian!

>>> print name_game('Flint', 'Fl') # Test 7: Initial F in consonant sound 'Fl'
Flint!
Flint, Flint, bo-bint,
Banana-fana fo-lint,
Fee-Fi-mo-mint,
Flint!

>>> print name_game('Chris', 'Chr') # Test 8: Three-letter consonant sound
Chris!
Chris, Chris, bo-bis,
Banana-fana fo-fis,
Fee-Fi-mo-mis,
Chris!

>>> print name_game('Marsha', 'M') # Test 9: Marsha, Marsha, Marsha!
Marsha!
Marsha, Marsha, bo-barsha,
Banana-fana fo-farsha,
Fee-Fi-mo-arsha,
Marsha!

>>> type(name_game('Katie', 'K')) # Test 10: Ensure strings are returned not printed
<type 'str'>


"""
#
#--------------------------------------------------------------------#



#-----Students' Solution---------------------------------------------#
#
#  Complete the task by filling in the template below.

from string import lstrip

def name_game(name,consonant):
	if consonant[:1].lower() in ['b','f','m']: # Get the first consonant, then force it to lower
		if consonant[1:].lower() in ['r','l']: # the annoying stuff, get the second consonant
			name_split = name[2:].lower() # well we won't be needing the first 2 letters of this name anymore
			first_letter = consonant[:1].lower() # I suffered a stroke here apparently, lets get the first letter of the consonant
			second_letter = consonant[1:2].lower() # and then the second. I don't know either.
			if consonant.lower() in ['fl','fr']: # now lets ignore both of those and just check if the overall consonant is one of these
				# Do you remember those splits I did earlier? Well lets put the second letter back, and then get rid of the first one.
				name_split = ['b'+name_split, lstrip('f'+second_letter+name_split,first_letter), 'm'+name_split]
				# Yep.
			if consonant.lower() == 'br': # and now lets do the same for our other 2 letter consonant test
				name_split = [lstrip('b'+second_letter+name_split,first_letter), 'f'+name_split, 'm'+name_split]
		else:
			# for everything else lets get rid of the first letter, then replace it with a new one in the most fabulous (terrible) way possible
			name_split = lstrip(name,consonant[:1]).lower()
			name_split = [lstrip('b'+name_split,consonant[:1].lower()), lstrip('f'+name_split,consonant[:1].lower()), lstrip('m'+name_split,consonant[:1].lower())]
	else:
		# and for every name that doesn't start with a b, f, m, or consonant at all, just get rid of that first letter and replace it with a new one.
		name_split = lstrip(name,consonant).lower()
		name_split = ['b'+name_split, 'f'+name_split, 'm'+name_split]

	# then return our string for doctests!
	return """{name}!\n{name}, {name}, bo-{name_split[0]},\nBanana-fana fo-{name_split[1]},\nFee-Fi-mo-{name_split[2]},\n{name}!""".format(name=name,name_split=name_split)


#
#--------------------------------------------------------------------#



#-----Automatic Testing----------------------------------------------#
#
#  The following code will automatically run the acceptance tests
#  when the program is "run".  If you want to prevent the tests
#  from running, and test your functions manually, comment out the
#  code below.
#

if __name__ == '__main__':
    from doctest import testmod
    testmod(verbose=True) # comment out this line to prevent testing

#
#--------------------------------------------------------------------#
